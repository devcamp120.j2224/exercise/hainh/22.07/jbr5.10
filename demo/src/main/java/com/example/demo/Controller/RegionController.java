package com.example.demo.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Region;
import com.example.demo.Service.RegionService;

@RestController
@RequestMapping("/")
public class RegionController {
    @Autowired
    private RegionService regionService;

    @GetMapping("/region-info")
    public Region getRegionInfo(@RequestParam("regionCode") String regionCode) {
            ArrayList<Region> regions = regionService.getRegionsAll();
        
            Region regionFind = new Region();
            for(Region region : regions) {
                if(region.getRegionCode().equals(regionCode)) {
                    regionFind = region;
                }
            }
            return regionFind;
    }
}
