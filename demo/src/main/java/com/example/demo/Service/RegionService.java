package com.example.demo.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Region;

@Service
public class RegionService {
    // biến static tạo 1 biến static  , tạo các thuộc tính trong static 
        Region dongnai = new Region("Đồng Nai", "60");
        Region saigon = new Region("Sài Gòn", "59");
        Region hanoi = new Region("Hà Nội", "29");
        Region texas = new Region("Texax", "430");
        Region florida = new Region("Florida", "305");
        Region newyork = new Region("New York", "718");
        Region okinawa = new Region("Okinawa", "495");
        Region tokyo = new Region("Tokyo", "4842");
        Region kyoto = new Region("Kyoto", "812");

        public ArrayList<Region> getRegionVN(){
            ArrayList<Region> regionsVN = new ArrayList<Region>();
    
           regionsVN.add(dongnai);
           regionsVN.add(saigon);
           regionsVN.add(hanoi);

            return regionsVN;
        }
        public ArrayList<Region> getRegionJP(){
            ArrayList<Region> regionsJP = new ArrayList<Region>();
    
            regionsJP.add(okinawa);
            regionsJP.add(tokyo);
            regionsJP.add(kyoto);
    
            return regionsJP;
        }
        public ArrayList<Region> getRegionUS(){
            ArrayList<Region> regionsUS = new ArrayList<Region>();
    
            regionsUS.add(texas);
            regionsUS.add(florida);
            regionsUS.add(newyork);
    
            return regionsUS;
        }
    
        public ArrayList<Region> getRegionsAll() {
            ArrayList<Region> regionsAll = new ArrayList<Region>();

            regionsAll.add(dongnai);
            regionsAll.add(saigon);
            regionsAll.add(hanoi);
            regionsAll.add(okinawa);
            regionsAll.add(tokyo);
            regionsAll.add(kyoto);
            regionsAll.add(texas);
            regionsAll.add(florida);
            regionsAll.add(newyork);
    
            return regionsAll;
            
        }
    }